#include <SPI.h>
#include <Streaming.h>
#include "ads1120.h"
#include "ad5270.h"

#define LED (13)
#define CS_POT (A3)
#define CS_ADC (A2)
#define DATA_READY (A1)
  
AD5270 pot(CS_POT);
ADS1120 adc(CS_ADC, DATA_READY);

// Convert a current in nA to the resistance 
// needed by a LM234.  Computed at 71 F using
// 214 uV/K * 295 K * 1.059 = 66854670 nA*ohm
#define CUR_TO_RES(I) (66854670L / (I)) 

// Converts a resistance in ohms to a 10 bit 
// dac output value for a 100 kohm AD5270.
// R * 2^10 / (100 kOhm)
#define RES_TO_DAC(R) ((R*1024L)/100000L)

// nA to DAC units
#define CUR_TO_DAC(I) (RES_TO_DAC(CUR_TO_RES(I)))

// These are the max/min voltages (in nV) that you
// can reliably read for a given gain
const int32_t ADC_VLIM[8] = {2000000000L,1000000000L,500000000L,200000000L,100000000L,60000000L,30000000L,15000000L};

#define NUM_GAINS_USED 3
const uint8_t GAINS_USED[NUM_GAINS_USED] = {0, 4, 7};

const uint8_t MUX_A_PINS[8] = {8, 9, 10, 11, 12, A4, A5, ATN};
const uint8_t MUX_B_PINS[8] = {0, 1, 2, 3, 4, 5, 6, 7};

const unsigned int NUM_PINS (128);
uint8_t pin_combo_status[NUM_PINS+1][NUM_PINS];

#define STATUSBIT_GAIN_FOUND (0)

// Range of pin indices on connector 1 that have short to ground resistors
#define CONN1_SHORT_PIN_MIN (115-1)
#define CONN1_SHORT_PIN_MAX (122-1)

void set_mux(const uint8_t* pins, uint8_t output)
{
  for (int i = 0; i < 8; i++)
  {
    digitalWrite(pins[i], (output >> i) & 1);
  }
}

#define set_mux_a(output) (set_mux(MUX_A_PINS, output))
#define set_mux_b(output) (set_mux(MUX_B_PINS, output))

#define ADC_TARGET_MUX (ADS1120_MUX_10)
#define ADC_TARGET_CURRENT (ADS1120_MUX_23)

void set_adc(uint8_t target, uint8_t gain)
{
  adc.reg_write_conf0(target, gain, ADS1120_PGA_ON);
  adc.data_start();
  delay(200);  // Let it settle
}

// Value of R6
#define CURRENT_CALIB_RESISTOR (1000L)
int32_t current_nA = 0;
int32_t offset_nV = 0;

void calibrate_adc()
{
  // Short the output so we know current is flowing
  set_mux_a(0);
  set_mux_b(0);

  // Read the voltage across the fixed resistor
  // to measure the current
  adc.spi_transaction_begin();
  set_adc(ADC_TARGET_CURRENT, ADS1120_GAIN_128);
  adc.data_start();
  adc.wait_for_data();
  int32_t result_nV = adc.data_read_nv();
  adc.spi_transaction_end();
  current_nA = result_nV / CURRENT_CALIB_RESISTOR;

  // Read the voltage for a shorted output to find
  // the resistance of the mux chips.
  adc.spi_transaction_begin();
  set_adc(ADC_TARGET_MUX, ADS1120_GAIN_128);
  adc.data_start();
  adc.wait_for_data();
  offset_nV = adc.data_read_nv();
  adc.spi_transaction_end();
}

void setup() 
{
  pinMode(LED, OUTPUT);
  pinMode(CS_POT, OUTPUT);
  pinMode(CS_ADC, OUTPUT);
  pinMode(DATA_READY, INPUT);
  
  digitalWrite(LED, HIGH);
  digitalWrite(CS_POT, HIGH);
  digitalWrite(CS_ADC, HIGH);

  for (int i = 0; i < 8; i++)
  {
    pinMode(MUX_A_PINS[i], OUTPUT);
    pinMode(MUX_B_PINS[i], OUTPUT);
  }
  
  Serial.begin(230400);
  SPI.begin();

  // Initialize the pot
  pot.configure();
  //pot.set_val(CUR_TO_DAC(2000));
  pot.set_val(CUR_TO_DAC(1000));  // nA

  // Initialize the ADC
  adc.spi_transaction_begin();
  adc.reg_write_conf1(ADS1120_DR_1000, ADS1120_MODE_NORMAL, ADS1120_CM_SINGLE, ADS1120_TS_OFF, ADS1120_BCS_OFF);
  adc.spi_transaction_end();

  calibrate_adc();
}

void loop() 
{  
  // Wait for a start signal
  digitalWrite(LED, LOW);
  while (!(Serial.available()) || Serial.read() != 's') {}
  while (!(Serial.available())) {}
  bool full_matrix = (bool)Serial.read();
  while (!(Serial.available())) {}
  bool target_self = (bool)Serial.read();
  digitalWrite(LED, HIGH);
  
  int start_time = millis();
  
  for (int i = 0; i < NUM_PINS + 1; i++)
  {
    for (int j = 0; j < NUM_PINS; j++)
    {
      pin_combo_status[i][j] = 0;
    }
  }
  
  adc.spi_transaction_begin();

  Serial << "calib," << current_nA << "," << offset_nV << endl;

  for (int ggg = 0; ggg < NUM_GAINS_USED; ggg++)
  {
    uint8_t gain = GAINS_USED[ggg];
    
    set_adc(ADC_TARGET_MUX, gain);
    Serial << "gain," << gain << endl;
        
    for (int bbb = 0; bbb < NUM_PINS; bbb++)
    {
      if (target_self)
        set_mux_b(bbb); // Connector 0
      else
        set_mux_b(bbb+128); // Connector 1

      // If we are targeting ourself (looking at cross shorts
      // on same connector) then aaa=bbb is meaningless and 
      // half of the matrix is redundant
      int a_start = 0;
      if (!full_matrix)
        a_start = bbb+1;

      // If this is one of our calibration short-to-ground resistors,
      // then don't bother reading the resistance between it and
      // anything except ground.  The answers will be wrong anyway,
      // and it just wastes time.
      if (!target_self && bbb >= CONN1_SHORT_PIN_MIN && bbb <= CONN1_SHORT_PIN_MAX)
        a_start = NUM_PINS;
      
      for (int aaa = a_start; aaa < NUM_PINS+1; aaa++)
      {           
          // Don't try again if we already were out of range
          // for a previous gain choice
          if (bitRead(pin_combo_status[aaa][bbb], STATUSBIT_GAIN_FOUND))
          {
            if (ggg == 0)
              Serial << "ERROR," << aaa << "," << bbb << endl;  
            continue;
          }
          
          set_mux_a(aaa);
          
          // Give the pins a moment to settle and take
          // an initial reading      
          delayMicroseconds(100);
          adc.data_start();
          adc.wait_for_data();
          int32_t result_nV = adc.data_read_nv();

          // If we didn't go out of range, then let's let it settle and read
          // for real this time.
          if ((result_nV <= ADC_VLIM[gain]) && (result_nV >= -ADC_VLIM[gain]))
          {
            delay(10);         
            adc.data_start();
            adc.wait_for_data();
            result_nV = adc.data_read_nv();
            if ((result_nV <= ADC_VLIM[gain]) && (result_nV >= -ADC_VLIM[gain]))
            {
              Serial << aaa << "," << bbb << "," << result_nV << endl;
              continue;
            }
          }

          // If we made it here then we must be out of range.  Mark the gain
          // as found so we don't keep trying.
          bitSet(pin_combo_status[aaa][bbb], STATUSBIT_GAIN_FOUND);

          // If we are out of range on the lowest gain choice then
          // we call it infinite and move on with life
          if (ggg == 0)
          { 
            if (result_nV > ADC_VLIM[gain])
              Serial << aaa << "," << bbb << "," << "+inf" << endl;
            else if (result_nV < -ADC_VLIM[gain])
              Serial << aaa << "," << bbb << "," << "-inf" << endl;
          }          
      }    
    }
  }
  
  adc.spi_transaction_end();

  Serial << "time," << millis() - start_time << endl;
  delay(1000);
}
