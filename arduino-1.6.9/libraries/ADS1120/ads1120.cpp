
#include "ads1120.h"

ADS1120::ADS1120(int pin_cs, int pin_drdy)
{
	this->gain = 0;
	this->pin_cs = pin_cs;
	this->pin_drdy = pin_drdy;
	pinMode(this->pin_drdy, INPUT);
	pinMode(this->pin_cs, OUTPUT);
	digitalWrite(this->pin_cs, HIGH);
}

void ADS1120::spi_transaction_begin()
{
	SPI.beginTransaction(SPISettings(ADS1120_SPI_SPEED, ADS1120_SPI_ORDER, ADS1120_SPI_MODE));
	digitalWrite(this->pin_cs, LOW);
	delayMicroseconds(10);  // Takes ~8 us for the pin to settle
}

void ADS1120::spi_transaction_end()
{
	digitalWrite(this->pin_cs, HIGH);
	SPI.endTransaction();
	delayMicroseconds(10);
}

void ADS1120::reg_write_conf0(uint8_t mux, uint8_t gain, uint8_t pga)
{
	this->gain = gain;
	this->reg_write(0, ((mux & ADS1120_BITMASK_MUX) << ADS1120_BITSHIFT_MUX) |
					   ((gain & ADS1120_BITMASK_GAIN) << ADS1120_BITSHIFT_GAIN) |
					   ((pga & ADS1120_BITMASK_PGA) << ADS1120_BITSHIFT_PGA));
}

void ADS1120::reg_write_conf1(uint8_t dr, uint8_t mode, uint8_t cm, uint8_t ts, uint8_t bcs)
{
	this->reg_write(1, ((dr & ADS1120_BITMASK_DR) << ADS1120_BITSHIFT_DR) |
					   ((mode & ADS1120_BITMASK_MODE) << ADS1120_BITSHIFT_MODE) |
					   ((cm & ADS1120_BITMASK_CM) << ADS1120_BITSHIFT_CM) |
					   ((ts & ADS1120_BITMASK_TS) << ADS1120_BITSHIFT_TS) |
					   ((bcs & ADS1120_BITMASK_BCS) << ADS1120_BITSHIFT_BCS));
}

uint8_t ADS1120::reg_read(uint8_t id)
{	
	SPI.transfer(ADS1120_CMD_RREG_ONE(id & 3));
	uint8_t to_return = SPI.transfer(ADS1120_CMD_NULL);

	return to_return;
}

void ADS1120::reg_write(uint8_t id, uint8_t val)
{	
	SPI.transfer(ADS1120_CMD_WREG_ONE(id & 3));
	SPI.transfer(val);
}

void ADS1120::data_start()
{
	SPI.transfer(ADS1120_CMD_START);
}

void ADS1120::wait_for_data()
{
  while (digitalRead(this->pin_drdy) != LOW) {}
}

int16_t ADS1120::data_read()
{
	uint8_t msb = SPI.transfer(0);
	uint8_t lsb = SPI.transfer(0);
	uint16_t result_unsigned = ((msb << 8) | lsb);
	
	// Interpret it as a 2's compliment number
	return static_cast<int16_t>(result_unsigned);
}

int32_t ADS1120::data_read_nv()
{
	int32_t raw = this->data_read();

	// raw*2.048*1.0e9/(32768.0*(2^gain));
	return (raw*62500L)>>(this->gain);
}

