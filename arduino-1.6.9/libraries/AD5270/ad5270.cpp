
#include "ad5270.h"

AD5270::AD5270(int pin_cs)
{
	this->pin_cs = pin_cs;
	pinMode(this->pin_cs, OUTPUT);
	digitalWrite(this->pin_cs, HIGH);	
}

void AD5270::configure()
{	
	this->write(AD5270_CMD_WRITE_ENABLE);
}

void AD5270::miso_highz()
{	
	// Put MISO into high impedance mode.  It seems I need to do this
	// every time I write out.
	this->write(0x8001);
	this->write(0x0000);
}

void AD5270::set_val(uint16_t val)
{	
	uint16_t cmd = AD5270_CMD_WRITE_RDAC | (AD5270_VAL_BITMASK & val);
	this->write(cmd);
	this->miso_highz();
}

void AD5270::write(uint16_t cmd)
{	
	SPI.beginTransaction(SPISettings(AD5270_SPI_SPEED, AD5270_SPI_ORDER, AD5270_SPI_MODE));
	digitalWrite(this->pin_cs, LOW);
	
	SPI.transfer16(cmd);
	
	digitalWrite(this->pin_cs, HIGH);
	SPI.endTransaction();
}

uint16_t AD5270::read(uint16_t cmd)
{	
	SPI.beginTransaction(SPISettings(AD5270_SPI_SPEED, AD5270_SPI_ORDER, AD5270_SPI_MODE));
	digitalWrite(this->pin_cs, LOW);
	
	SPI.transfer16(cmd);
	uint16_t result = SPI.transfer16(0);
	
	digitalWrite(this->pin_cs, HIGH);
	SPI.endTransaction();
	
	return result;
}

uint16_t AD5270::read_ctrl()
{
	uint16_t result = this->read(AD5270_CMD_READ_CTRL);
	this->miso_highz();
	return result;
}

uint16_t AD5270::read_rdac()
{
	uint16_t result = this->read(AD5270_CMD_READ_RDAC);
	this->miso_highz();
	return result;
}
