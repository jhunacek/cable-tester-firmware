#ifndef AD5270_h
#define AD5270_h

#include <Arduino.h>
#include <SPI.h>

#define AD5270_SPI_SPEED (100000)
#define AD5270_SPI_ORDER (MSBFIRST)
#define AD5270_SPI_MODE (SPI_MODE1)

#define AD5270_VAL_BITMASK (0b0000001111111111)
#define AD5270_CMD_WRITE_RDAC (0b0000010000000000)
#define AD5270_CMD_READ  (0b0000100000000000)
#define AD5270_CMD_WRITE_ENABLE  (0b0001110000000010)
#define AD5270_CMD_READ_CTRL  (0b0000100000000000)
#define AD5270_CMD_READ_RDAC  (0b0010000000000000)

class AD5270
{
	public:
		AD5270(int pin_cs);
		void set_val(uint16_t val);
		void write(uint16_t cmd);
		uint16_t read(uint16_t cmd);
		uint16_t read_ctrl();
		uint16_t read_rdac();
		void configure();
		void miso_highz();
		int pin_cs;
};

#endif

